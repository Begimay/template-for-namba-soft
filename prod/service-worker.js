/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [["/css/libs.min.css","5e11b323aad8aad6a601a71f485006b9"],["/css/style.min.css","57454a6eeb64056b6d9860d72382e76a"],["/img/gif/preloader.gif","126ca6bcc2616e4edf09f466e9925396"],["/img/img.jpg","5be00481b9173fa3593394128a41cbc5"],["/img/jpg/g1.jpg","608029996605bd19910e4766380639aa"],["/img/jpg/g10.jpg","9fa3121808d7dca85f008cb74986aaa3"],["/img/jpg/g11.jpg","78e51b2136bf587006f72d3b32f9244f"],["/img/jpg/g12.jpg","e6c5c3ee7fc614ced1099eed86623873"],["/img/jpg/g2.jpg","df9e090e8a7fdc1fa1c71fdc278f4159"],["/img/jpg/g3.jpg","10604a7d6c414fdc1499acac784deeb7"],["/img/jpg/g4.jpg","2fac37579194a4b2e84c05df9e18761d"],["/img/jpg/g5.jpg","7230bfed8227aca184ff666f8b359331"],["/img/jpg/g6.jpg","7242583d01eca30a3b581c78d9abe36e"],["/img/jpg/g7.jpg","d3148ef040ea6567e9121d0818f19c64"],["/img/jpg/g8.jpg","6b6589324d3f2b1b39d2c38fbc425ea7"],["/img/jpg/g9.jpg","f16f86ec3b8bc3c2acf50b8ebcf70d5d"],["/img/jpg/sponsor3.jpg","e19fc9ba98b17d68a6ec54969372dec0"],["/img/png/ab1.png","9ee0195e0d4fdb99cc94756d9008fee7"],["/img/png/ab2.png","72eb8d3b5764068e0b87deb3795a7a8e"],["/img/png/ab3.png","fae71e2bac8961d98357c6c7ce7ff9b8"],["/img/png/foot1.png","b5febdbfe04c53e306691f82eb8e8f9d"],["/img/png/logo.png","2e9163a235470081ab0c1024d8aa7380"],["/img/png/m1.png","5f18c78873ff6d5c8949f4a3cd31aabb"],["/img/png/m2.png","5abfb70e41357dea7038188cf16b158a"],["/img/png/m3.png","51f1eda622b297c38e7c3fda4db97097"],["/img/png/m4.png","ffa411de4f07ab39726292513897b198"],["/img/png/m5.png","70e2309c752a28124f97dca854e46d9e"],["/img/png/m6.png","f0e69f14fc6d73e81657e02b08b5e1ee"],["/img/png/m7.png","8b051c8589f2fab88ac996ac401becaa"],["/img/png/m8.png","14f355caf699402997c3d7866830a7cc"],["/img/png/m9.png","3ccea41cf824957758b74c0dcd09465c"],["/img/png/post1.png","21a1af679ad6693a41448d0e1f43fb59"],["/img/png/post2.png","029d8885a9db7064ef4626e83eff5173"],["/img/png/sl1.png","434b1b3a6c2d85a5e8c75fa73409322b"],["/img/png/sl2.png","a08367e5871b699f9a542384d64ef47f"],["/img/png/sl3.png","3d5c73e2e4e70f1677ed8350af85d58b"],["/img/png/sl4.png","b292070adfede66866089e3bdbea32dc"],["/img/png/sponsor1.png","823f608fd8469bf89c74b5dcc36adf4b"],["/img/png/sponsor2.png","6ce4d6048fd18cbfcda944d552e60ba6"],["/img/png/sponsor3.png","b8b944fa2bbb495831081cf0b6425e1a"],["/img/png/sponsor4.png","2572553535f7ae53c1d0ef48f244861e"],["/img/png/sponsor5.png","957f228e35efeb79aa5cc929b8b696d5"],["/index.html","097575677be3f8e45dd50c4a3a19c517"],["/js/common.min.js","a34a8504bdea8d7f7f8ce0987ba600ee"],["/js/libs.min.js","2066d02863843c798ee0df36e57d3588"],["/manifest.json","cd97fb57f20e6b285fc5398738a312e3"]];
var cacheName = 'sw-precache-v3--' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function (originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function (originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function (originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function (whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function (originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {
      
      // Force the SW to transition from installing -> active state
      return self.skipWaiting();
      
    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});







